//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/    /*Command*/         /*Update Interval*/    /*Update Signal*/
    {"", "cmus_dwm",        1,		5},
	{"", "kbb",				1,		4},
	{"", "vpn_status",		1,		13},
	{"", "net-speed",		1,		3},
	{"", "arch_update",		0,		11},
	{"", "weather_status",	1800,	12},
	{"", "cputemp",         5,		6},
	{"", "volume",          0,		10},
	{"", "battery",         1,		2},
    {"", "datetime",        1,		1},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
